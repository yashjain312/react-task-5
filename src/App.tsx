import React from "react";
import "./App.css";
import { Customer } from "./types";
import faker from "faker";
import { map } from "lodash-es";
import ListItem from "./components/ListItem/ListItem";

const customers = map(
  Array(30),
  () =>
    ({
      id: faker.random.uuid(),
      profileImage: faker.image.image(),
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      email: faker.internet.email(),
      phone: faker.phone.phoneNumber(),
      country: faker.address.county(),
      active: faker.random.boolean(),
    } as Customer)
);

const App: React.FC = () => {
  return (
    <div className="layout">
      <div className="title">
        <h2>Customers</h2>
      </div>
      <ul>
        {customers.map((c) => (
          <ListItem customer={c} key={c.id}/>
        ))}
      </ul>
    </div>
  );
};

export default App;
