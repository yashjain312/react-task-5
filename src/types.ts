export interface Customer {
  id: string;
  name: string;
  email: string;
  phone: string;
  country: string;
  active: boolean;
  profileImage: string;
}
